package br.com.mauda.bilhetesAereos.exception;


/**
 * Classe de Exception para o projeto de Bilhetes Aereos
 * @author Mauda
 *
 */

public class BilhetesAereosException extends RuntimeException{

	private static final long serialVersionUID = 4928599035264976611L;
	
	public BilhetesAereosException(String message) {
		super(message);
	}
	
	public BilhetesAereosException(Throwable t) {
		super(t);
	}
}
