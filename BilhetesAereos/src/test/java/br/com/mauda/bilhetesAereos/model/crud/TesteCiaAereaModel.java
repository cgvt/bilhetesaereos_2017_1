package br.com.mauda.bilhetesAereos.model.crud;

import java.util.Collection;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import br.com.mauda.bilhetesAereos.massaTestes.MassaTestesCiaAereaEnum;
import br.com.mauda.bilhetesAereos.model.CiaAerea;
import br.com.mauda.bilhetesAereos.modificadores.CiaAereaCreator;
import br.com.mauda.bilhetesAereos.verificador.CiaAereaVerificator;

@RunWith(Parameterized.class)
public class TesteCiaAereaModel extends AbstractCrudTestModel<CiaAerea, MassaTestesCiaAereaEnum> {
	
	@SuppressWarnings("rawtypes")
	@Parameterized.Parameters
	public static Collection listaObjetos() {
		return MassaTestesCiaAereaEnum.getParameters();
	}	
	
	// ////////////////////////////////////////
	// CONSTRUTORES
	// ////////////////////////////////////////

	public TesteCiaAereaModel(MassaTestesCiaAereaEnum ciaAereaEnum) {
		super(ciaAereaEnum);
		creator = CiaAereaCreator.getInstance();
		verificator = CiaAereaVerificator.getInstance();
		objectEnumTemp = MassaTestesCiaAereaEnum.getInstance();
	}	
}